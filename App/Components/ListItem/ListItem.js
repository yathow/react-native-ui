import React from 'react';
import { ListItem as RNEListItem } from 'react-native-elements';

const ListItem = props => {
  return <RNEListItem {...props} />;
};

export default ListItem;
