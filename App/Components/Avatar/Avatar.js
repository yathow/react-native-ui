import React from 'react';
import { Avatar as RNEAvatar } from 'react-native-elements';

const Avatar = props => {
  return <RNEAvatar {...props} />;
};

export default Avatar;
