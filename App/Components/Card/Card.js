import { Card as RNECard } from 'react-native-elements';
import React from 'react';

const Card = props => {
  return <RNECard {...props} />;
};

export default Card;
