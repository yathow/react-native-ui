import RNMModal from 'react-native-modal';
import React from 'react';
import Style from './ModalStyle';
import View from '../View/View';
import { withTheme } from 'react-native-elements';

const Modal = props => {
  const nProps = {
    ...props
  };
  delete nProps['containerStyle'];
  const nContainerStyle = {
    ...Style.container(props.theme),
    ...props.containerStyle
  };
  return (
    <RNMModal {...nProps}>
      <View style={nContainerStyle}>{props.children}</View>
    </RNMModal>
  );
};

export default withTheme(Modal);
