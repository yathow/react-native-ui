import Metrics from '../../Theme/Metrics';

const Styles = {
  container: theme => ({
    ...theme.Modal.containerStyle
  })
};

export default Styles;
