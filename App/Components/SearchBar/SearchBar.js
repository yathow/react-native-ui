import React from 'react';
import { SearchBar as RNESearchBar } from 'react-native-elements';

const SearchBar = props => {
  return <RNESearchBar {...props} />;
};

export default SearchBar;
