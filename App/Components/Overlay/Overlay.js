import { Overlay as RNEOverlay } from 'react-native-elements';
import React from 'react';

const Overlay = props => {
  return <RNEOverlay {...props}>{props.children}</RNEOverlay>;
};

export default Overlay;
