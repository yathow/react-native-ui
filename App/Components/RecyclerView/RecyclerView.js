import { DataProvider, RecyclerListView as RNRecyclerListView } from 'recyclerlistview';
import { LoadingIndicator, View } from '@merchstack/react-native-ui';

import { PureComponent } from 'react';
import React from 'react';
import Style from './RecyclerViewStyle';

class RecyclerView extends PureComponent {
  constructor() {
    super();

    this.dataProvider = new DataProvider((r1, r2) => {
      return r1 !== r2;
    });

    this.state = {
      dataProvider: this.dataProvider.cloneWithRows([])
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      dataProvider: this.dataProvider.cloneWithRows(nextProps.data)
    });
  }

  render() {
    const nProps = {...this.props};
    delete nProps.data;
    delete nProps.isLoading;
    delete nProps.ListEmptyComponent;

    if (this.props.data.length > 0) {
      return <RNRecyclerListView {...nProps} 
        dataProvider={this.state.dataProvider}
        renderFooter={this.renderFooter}/>;
    } else if (this.props.isLoading) {
      return this.renderLoading();
    } else {
      return this.renderEmptyList();
    }
  };

  renderFooter = () => {
    return <View style={Style.footerPanel}>
      { this.renderLoading() }
      { this.props.renderFooter ? this.props.renderFooter() : null}
    </View>;
  }

  renderLoading = () => {
    if (this.props.isLoading) {
      return <LoadingIndicator style={Style.loading} size='large'/>
    } else {
      return null;
    } 
  }

  renderEmptyList = () => {
    if (this.props.renderEmptyList !== null) {
      return this.props.renderEmptyList;
    } else {
      return null;
    }
  }
}

export default RecyclerView;