import { ActivityIndicator, View } from 'react-native';

import React from 'react';
import { withTheme } from 'react-native-elements';

const LoadingIndicator = props => {
  let nProps = { ...props };
  delete nProps.color;
  return <ActivityIndicator color={props.theme.colors.primary} {...nProps} />;
};

export default withTheme(LoadingIndicator);
