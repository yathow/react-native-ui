import { Picker as RNPicker } from 'react-native';
import { withTheme } from 'react-native-elements';

class Picker extends RNPicker {
  constructor(props) {
    super();
  }
}

export default withTheme(Picker);
