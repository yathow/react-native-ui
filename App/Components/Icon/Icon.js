import React from 'react';
import { Icon as RNEIcon } from 'react-native-elements';

const Icon = props => {
  return <RNEIcon {...props} />;
};

export default Icon;
