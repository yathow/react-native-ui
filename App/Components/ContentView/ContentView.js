import React from 'react';
import Snackbar from '../Snackbar/Snackbar';
import { View } from 'react-native';
import { withTheme } from 'react-native-elements';

const ContentView = props => {
  const renderSnackbars = () => (
    <View>
      <Snackbar status="info" text={props.infoMessage} />
      <Snackbar status="warning" text={props.warningMessage} />
      <Snackbar status="error" text={props.errorMessage} />
      <Snackbar status="success" text={props.successMessage} />
    </View>
  );

  const renderContent = () => <View style={{ flex: 1 }}>{props.children}</View>;

  const getPosition = () => {
    let position = 'bottom'; // default value
    if (props.position) {
      position = props.position; // get from properties
    } else if (props.theme.ContentView && props.theme.ContentView.position) {
      position = props.theme.ContentView.position; // get from theme
    }
    return position;
  };

  const nProps = { ...props };
  delete nProps.infoMessage;
  delete nProps.warningMessage;
  delete nProps.errorMessage;
  delete nProps.successMessage;
  delete nProps.position;

  if (getPosition() === 'top') {
    return (
      <View {...nProps}>
        {renderSnackbars()}
        {renderContent()}
      </View>
    );
  } else {
    return (
      <View {...nProps}>
        {renderContent()}
        {renderSnackbars()}
      </View>
    );
  }
};

export default withTheme(ContentView);
