import React from 'react';
import { Image as RNEImage } from 'react-native-elements';
import { TouchableWithoutFeedback } from 'react-native';

const Image = (props) => {
  const { onPress } = props;

  const nProps = { ...props };
  delete nProps.onPress;

  var component = <RNEImage {...nProps} />;
  if (onPress) {
    // Wrap a touchable wrapper if required
    component = <TouchableWithoutFeedback onPress={onPress}>{component}</TouchableWithoutFeedback>;
  }

  return component;
};

export default Image;
