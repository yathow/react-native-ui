import React from 'react';
import { Badge as RNEBadge, withBadge } from 'react-native-elements';

const Badge = props => {
  return <RNEBadge {...props} />;
};

export default Badge;
export { withBadge };
