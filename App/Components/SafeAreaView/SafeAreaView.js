import React, { PureComponent } from 'react';

import { SafeAreaView as RNSafeAreaView } from 'react-navigation';
import { withTheme } from 'react-native-elements';

class SafeAreaView extends PureComponent {
  render() {
    return (
      <RNSafeAreaView {...this.getProps()} style={this.getStyle()}>
        {this.props.children}
      </RNSafeAreaView>
    );
  }

  getProps() {
    const props = { ...this.props };
    delete props.style;
    return { ...props };
  }

  getStyle() {
    let componentStyle =
      this.props.theme && this.props.theme.SafeAreaView ? this.props.theme.SafeAreaView : null;
    let propStyle = this.props.style ? this.props.style : null;
    return {
      ...componentStyle,
      ...propStyle
    };
  }
}

export default withTheme(SafeAreaView);
