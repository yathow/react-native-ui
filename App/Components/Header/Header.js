import React from 'react';
import { Header as RNEHeader } from 'react-native-elements';

const Header = props => {
  return <RNEHeader {...props} />;
};

export default Header;
