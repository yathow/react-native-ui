import { LoadingIndicator, View } from '@merchstack/react-native-ui';

import { FlatList as RNFlatList } from 'react-native';
import React from 'react';
import Style from './FlatListStyle';

const FlatList = props => {
  let nProps = { ...props };
  delete nProps.ListFooterComponent;
  delete nProps.isLoading;

  const footer = () => {
    return <View style={Style.footerPanel}>
      { (props.isLoading) ? 
      <LoadingIndicator style={Style.loading} size='large'/> : null }
      { props.ListFooterComponent ? props.ListFooterComponent() : null}
    </View>;
  }
  return <RNFlatList {...nProps} ListFooterComponent={footer}/>;
};

export default FlatList;
