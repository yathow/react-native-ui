import { TextInput, View } from 'react-native';

import { Button } from '../Button';
import { PureComponent } from 'react';
import React from 'react';
import Style from './NumberPickerStyle';
import { withTheme } from 'react-native-elements';

class NumberPicker extends PureComponent {
  render() {
    const style = this.props.style ? this.props.style : {};
    return (
      <View style={Style.container(this.props.theme, style)}>
        <Button
          buttonStyle={Style.btnDecr(this.props.theme)}
          title="-"
          onPress={this.props.decrement}
        />
        <TextInput
          keyboardType="numeric"
          textAlign="right"
          style={Style.input}
          onChangeText={this.props.onChangeText}
          value={this.props.value}
        />
        <Button
          buttonStyle={Style.btnIncr(this.props.theme)}
          title="+"
          onPress={this.props.increment}
        />
      </View>
    );
  }
}

export default withTheme(NumberPicker);
