import { Fonts } from '../../Theme';
import Metrics from '../../Theme/Metrics';

const Styles = {
  container: (theme, style) => ({
    flexDirection: 'row',
    marginLeft: Metrics.md,
    marginRight: Metrics.md,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: theme.colors.inputBorder,
    height: Fonts.input + 2 * Metrics.md,
    ...style,
  }),
  btnDecr: (theme) => ({
    borderRadius: 0,
    padding: Metrics.sm,
    width: 22,
    borderRightWidth: 1,
    borderColor: theme.colors.inputBorder,
    height: '100%',
  }),
  btnIncr: (theme) => ({
    borderRadius: 0,
    padding: Metrics.sm,
    width: 22,
    borderLeftWidth: 1,
    borderColor: theme.colors.inputBorder,
    height: '100%',
  }),
  input: {
    flex: 1,
    fontSize: Fonts.input,
    padding: 0,
    marginLeft: Metrics.md,
    marginRight: Metrics.md,
    marginTop: 0,
    marginBottom: 0,
  },
};

export default Styles;
