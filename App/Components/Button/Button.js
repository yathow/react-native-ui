import { Button as RNEButton, withTheme } from 'react-native-elements';

import React from 'react';
import Styles from './ButtonStyle';

const Button = props => {
  const { theme, buttonStyle, status, inputStyle } = props;
  return (
    <RNEButton
      {...props}
      titleStyle={Styles.titleStyle(theme, status, inputStyle)}
      buttonStyle={Styles.buttonStyle(theme, status, buttonStyle)}
    />
  );
};

export default withTheme(Button);
