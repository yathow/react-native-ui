import Metrics from '../../Theme/Metrics';

const Styles = {
  titleStyle: (theme, status, titleStyle) => {
    const baseStyle = {};
    if (
      theme != null &&
      theme.Button != null &&
      theme.Button[status] != null &&
      theme.Button[status].textColor != null
    ) {
      baseStyle.color = theme.Button[status].textColor;
    }
    return {
      ...baseStyle,
      ...titleStyle
    };
  },
  buttonStyle: (theme, status, buttonStyle) => {
    const baseStyle = {};
    if (
      theme != null &&
      theme.Button != null &&
      theme.Button[status] != null &&
      theme.Button[status].backgroundColor != null
    ) {
      baseStyle.backgroundColor = theme.Button[status].backgroundColor;
    }
    return {
      ...baseStyle,
      ...buttonStyle
    };
  }
};

export default Styles;
