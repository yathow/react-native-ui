import { Metrics } from '../../Theme';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  statusText: (status, theme, style, highlight) => {
    const baseStyle = {};
    if (theme) {
      if (theme.colors && theme.colors[status]) {
        baseStyle.color = highlight ? theme.backgroundColors[status] : theme.colors[status];
      }
      if (theme.backgroundColors && theme.backgroundColors[status]) {
        baseStyle.backgroundColor = highlight
          ? theme.colors[status]
          : theme.backgroundColors[status];
      }
    }
    return {
      ...baseStyle,
      ...style
    };
  }
});
