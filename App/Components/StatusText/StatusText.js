import React, { Component } from 'react';

import Style from './StatusTextStyle';
import Text from '../Text/Text';
import { withTheme } from 'react-native-elements';

const StatusText = props => {
  const { status, style, children, theme, highlight } = props;
  return <Text style={Style.statusText(status, theme, style, highlight)}>{children}</Text>;
};

export default withTheme(StatusText);
