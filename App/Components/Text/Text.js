import React from 'react';
import { Text as RNEText } from 'react-native-elements';

const Text = props => {
  return <RNEText {...props} />;
};

export default Text;
