import React from 'react';
import { View } from 'react-native';
import { withTheme } from 'react-native-elements';
import SearchBar from '../SearchBar/SearchBar';
import Styles from './SearchBarHeaderStyle';

const SearchBarHeader = props => {
  const { theme } = props;
  return (
    <View style={Styles.header(theme)}>
      <SearchBar {...props} />
    </View>
  );
};

export default withTheme(SearchBarHeader);
