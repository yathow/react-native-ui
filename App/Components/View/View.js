import React from 'react';
import { View as RNView } from 'react-native';

const View = props => {
  return <RNView {...props}>{props.children}</RNView>;
};

export default View;
