import { Divider as RNEDivider } from 'react-native-elements';
import React from 'react';

const Divider = props => {
  return <RNEDivider {...props} />;
};

export default Divider;
