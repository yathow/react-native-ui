import React from 'react';
import { Text } from '../Text/Text';

const Link = props => {
  let nProps = {
    ...props
  };
  delete nProps.style;

  const style = {
    ...props.style
  };

  return <Text {...nProps} style={style} />;
};

export default Link;
