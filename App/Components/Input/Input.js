import { Input as RNEInput, withTheme } from 'react-native-elements';

import { PureComponent } from 'react';
import React from 'react';
import Style from './InputStyle';
import Text from '../Text/Text';
import { View } from 'react-native';

class Input extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      focus: false,
      text: props.value ? props.value : '',
      showTitle: props.value && props.value.length > 0,
    }
  }

  showTitle() {
    return this.state.focus || this.state.text !== '';
  }

  onChangeText(text) {
    const newState = {
      ...this.state,
      text: text,
      showTitle: text.length > 0 || this.state.focus,
    };

    this.setState(newState);
    if (this.props.onChangeText != null) {
      this.props.onChangeText(text);
    }
  }

  onFocus(event) {
    const newState = {
      ...this.state,
      focus: true,
      showTitle: true,
    };

    this.setState(newState);
    if (this.props.onFocus != null) {
      this.props.onFocus(event);
    }
  }

  onBlur(event) {
    const newState = {
      ...this.state,
      focus: false,
      showTitle: this.state.text.length > 0,
    };

    this.setState(newState);
    if (this.props.onBlur != null) {
      this.props.onBlur(event);
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.value !== this.props.value) {
      const { value }  = this.props;
      this.setState({ 
        ...this.state, 
        text: value ? value : '', 
        showTitle: (value && value.length > 0) || this.state.focus
      });
    }
  }

  render() { 
    const { theme, inputStyle } = this.props;

    var cInputStyle = {
      ...Style.inputStyle(theme, this.state.focus),
      ...inputStyle,
    };

    if (this.isFloatingPlaceholder()) {
      const props = this.getInputProperties();
      return (
        <View style={Style.titleContainer}>
          <RNEInput {...props} inputStyle={cInputStyle} />
          {this.renderPlaceholderTitle()}
        </View>
      );
    } else {
      return <RNEInput {...this.props} inputStyle={cInputStyle} />;
    }
  }

  isFloatingPlaceholder() {
    let status = this.props.placeholder != null;
    if (status) {
      if (!isNaN(this.props.floatingTitle)) {
        status = this.props.floatingTitle;
      } else {
        status = this.getDefaultFloatingTitle();
      }
    }
    return status;
  }

  getDefaultFloatingTitle() {
    let status = false;
    if (this.props.theme.Input && this.props.theme.Input.floatingTitle) {
      status = this.props.theme.Input.floatingTitle;
    }
    return status;
  }

  getInputProperties() {
    let props = {
      ...this.props,
      onChangeText: (text) => this.onChangeText(text),
      onFocus: (event) => this.onFocus(event),
      onBlur: (event) => this.onBlur(event),
      placeholder: this.showTitle() ? '' : this.props.placeholder,
    };
    delete props['floatingTitle'];
    return props;
  }

  renderPlaceholderTitle() {
    if (this.state.showTitle) {
      return (
        <Text style={Style.title(this.props.theme, this.state.focus)}>
          {this.props.placeholder}
        </Text>
      );
    } else {
      return null;
    }
  }
}

export default withTheme(Input);