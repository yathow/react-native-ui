import Fonts from '../../Theme/Fonts';
import Metrics from '../../Theme/Metrics';

const Styles = {
  title: (theme, focus) => ({
    position: 'absolute',
    color: focus ? theme.colors.highlight : theme.colors.inputBorder,
    marginLeft: Metrics.lg,
    paddingLeft: Metrics.sm,
    paddingRight: Metrics.sm,
    backgroundColor: theme.backgroundColors.main,
    fontWeight: 'bold',
    fontSize: Fonts.input,
  }),
  titleContainer: {
    paddingTop: Fonts.input / 3,
  },
  inputStyle: (theme, focus) => ({
    ...(focus ? theme.Input.highlightInputStyle : theme.Input.inputStyle),
  }),
};

export default Styles;
