import React from 'react';
import StatusText from '../StatusText/StatusText';
import Styles from './SnackbarStyle';
import { withTheme } from 'react-native-elements';

const Snackbar = props => {
  const { theme, status } = props;
  if (Array.isArray(props.text)) {
    const show = props.text != null && props.text.length > 0;
    return show ? (
      <StatusText status={status} style={Styles.snackbar}>
        {props.text[0]}
      </StatusText>
    ) : null;
  } else {
    const show = props.text != null && props.text !== '';
    return show ? (
      <StatusText status={status} style={Styles.snackbar}>
        {props.text}
      </StatusText>
    ) : null;
  }
};

export default withTheme(Snackbar);
