import Metrics from '../../Theme/Metrics';

const Styles = {
  snackbar: {
    padding: 15,
    fontWeight: 'bold'
  }
};

export default Styles;
