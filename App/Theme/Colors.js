/**
 * This file contains the application's colors.
 *
 * Define color here instead of duplicating them throughout the components.
 * That allows to change them more easily later on.
 */

export default {
  transparent: 'rgba(0,0,0,0)',

  text: '#212529',
  primary: '#007bff',
  success: '#28a745',
  error: '#dc3545',

  gray1: '#d6d6d6',
  gray2: '#f9f9f9',
  gray3: '#a9a9a9',
  gray4: '#f6f6f6',
  blue1: '#3498DB',
  blue2: '#cce5ff',
  blue3: '#55accf',
  white: '#ffffff',
  black: '#000000',
  red1: '#dc3545',
  lightblue1: '#C7EBF9',

  pink1: '#f8d7da',
  orange1: '#FFA500',
  orange2: '#FF8C00',
  orange3: '#ffae42',
  yellow1: '#fff3cd',
  green1: '#155724',
  green2: '#d4edda',

  purple1: '#A569BD'
};
