import Colors from './Colors';
import Fonts from './Fonts';
import Metrics from './Metrics';
import { StyleSheet } from 'react-native';

const baseColors = {
  primary: Colors.blue1,
  secondary: Colors.black,
  inverse: Colors.white,
  button: Colors.black,
  input: Colors.black,
  inputBorder: Colors.gray3,
  highlight: Colors.orange2,
  link: Colors.blue1,

  text: Colors.black,
  muted: Colors.gray3,
  info: Colors.blue1,
  success: Colors.green1,
  error: Colors.red1,
  warning: Colors.orange1,
};

const baseBackgroundColors = {
  primary: Colors.blue2,
  secondary: Colors.black,

  input: Colors.white,
  button: Colors.gray1,
  panel: Colors.gray1,

  odd: Colors.gray4,
  even: Colors.white,

  info: Colors.blue2,
  success: Colors.green2,
  error: Colors.pink1,
  warning: Colors.yellow1,
};

const baseConfig = {
  colors: baseColors,
  backgroundColors: baseBackgroundColors,
  metrics: Metrics,
  fonts: Fonts,
};

const createConfig = (nConfig = {}) => {
  const { colors, backgroundColors, metrics, fonts } = nConfig;

  const config = {
    ...baseConfig,
  };

  if (nConfig && colors) {
    config.colors = StyleSheet.flatten([config.colors, colors]);
  }

  if (nConfig && backgroundColors) {
    config.backgroundColors = StyleSheet.flatten([config.backgroundColors, backgroundColors]);
  }

  if (nConfig && metrics) {
    config.metrics = StyleSheet.flatten([config.metrics, metrics]);
  }

  if (nConfig && fonts) {
    config.fonts = StyleSheet.flatten([config.fonts, fonts]);
  }

  return config;
};

const createTheme = (nConfig) => {
  const config = createConfig(nConfig);
  return {
    ...config,
    Avatar: {
      overlayContainerStyle: {
        backgroundColor: config.colors.primary,
      },
    },
    Header: {
      backgroundColor: config.colors.primary,
      containerStyle: {
        justifyContent: 'space-around',
      },
    },
    Card: {
      titleStyle: {
        ...config.fonts.h4,
        color: config.colors.secondary,
        textAlign: 'left',
        marginBottom: config.metrics.sm,
      },
      containerStyle: {
        padding: config.metrics.md,
        margin: config.metrics.sm,
        marginBottom: config.metrics.sm,
        marginTop: 0,
        borderRadius: config.metrics.borderRadius,
        borderColor: config.backgroundColors.panel,
      },
    },
    Button: {
      raised: true,
      buttonStyle: {
        borderRadius: config.metrics.borderRadius,
        backgroundColor: config.backgroundColors.button,
      },
      titleStyle: {
        color: Colors.black,
        fontSize: config.fonts.input,
      },
      primary: {
        backgroundColor: config.colors.primary,
        textColor: config.colors.inverse,
      },
      secondary: {
        backgroundColor: config.colors.secondary,
        textColor: config.colors.inverse,
      },
      info: {
        backgroundColor: config.colors.info,
        textColor: config.colors.inverse,
      },
      danger: {
        backgroundColor: config.colors.error,
        textColor: config.colors.inverse,
      },
    },
    Image: {
      placeholderStyle: {
        backgroundColor: Colors.transparent,
      },
    },
    Modal: {
      containerStyle: {
        padding: config.metrics.md,
        backgroundColor: config.backgroundColors.main,
      },
    },
    Text: {
      style: {
        color: config.colors.text,
        fontSize: config.fonts.regular,
      },
      h1Style: {
        ...config.fonts.h1,
        color: config.colors.primary,
      },
      h2Style: {
        ...config.fonts.h2,
        color: config.colors.secondary,
      },
      h3Style: {
        ...config.fonts.h3,
        color: config.colors.primary,
      },
      h4Style: {
        ...config.fonts.h4,
        color: config.colors.secondary,
      },
    },
    ListItem: {
      containerStyle: {
        backgroundColor: config.backgroundColors.main,
      },
      titleStyle: {
        fontSize: config.fonts.h4.fontSize,
      },
    },
    Divider: {
      marginTop: config.metrics.sm,
      marginBottom: config.metrics.sm,
      backgroundColor: config.colors.inputBorder,
    },
    SearchBar: {
      containerStyle: {
        backgroundColor: Colors.transparent,
        borderTopWidth: 0,
        borderBottomWidth: 0,
      },
      inputContainerStyle: {
        backgroundColor: Colors.white,
        borderRadius: config.metrics.borderRadius,
      },
      inputStyle: {
        color: Colors.black,
        backgroundColor: Colors.transparent,
        padding: 0,
        borderWidth: 0,
        borderRadius: 0,
      },
    },
    Input: {
      marginTop: config.metrics.sm,
      marginBottom: config.metrics.sm,
      inputContainerStyle: {
        borderBottomWidth: 0,
      },
      inputStyle: {
        fontSize: config.fonts.input,
        borderRadius: config.metrics.borderRadius,
        borderWidth: 1,
        paddingLeft: config.metrics.md,
        paddingRight: config.metrics.md,
        borderColor: config.colors.inputBorder,
        color: config.colors.input,
        backgroundColor: config.backgroundColors.input,
      },
      highlightInputStyle: {
        fontSize: config.fonts.input,
        borderRadius: config.metrics.borderRadius,
        borderWidth: 1,
        paddingLeft: config.metrics.md,
        paddingRight: config.metrics.md,
        borderColor: config.colors.highlight,
        color: config.colors.input,
        backgroundColor: config.backgroundColors.input,
      },
      errorStyle: {
        color: config.colors.error,
      },
    },
    TabStyle: {
      backgroundColor: config.colors.primary,
      fontSize: config.fonts.regular,
    },
    Picker: {
      containerStyle: {
        backgroundColor: 'red',
      },
    },
    SafeAreaView: {
      backgroundColor: config.backgroundColors.main,
    },
    ContentView: {
      position: 'bottom',
    },
    Overlay: {
      overlayBackgroundColor: config.backgroundColors.main,
    },
  };
};

export default createTheme;
