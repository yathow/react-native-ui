import { Dimensions, PixelRatio } from 'react-native';

/**
 * This file contains metric values that are global to the application.
 */

export default {
  // Examples of metrics you can define:
  // baseMargin: 10,
  // largeMargin: 20,
  // smallMargin: 5,
  xl: 25,
  lg: 20,
  md: 10,
  sm: 5,
  xs: 2,

  smBorderRadius: 5,
  borderRadius: 10,
  spacer: 100,

  screenWidth: () => Dimensions.get('window').width,
  screenHeight: () => Dimensions.get('window').height,

  screenWidthDpi: () => Dimensions.get('window').width * PixelRatio.get(),
  screenWidthDpi: () => Dimensions.get('window').height * PixelRatio.get(),
};
