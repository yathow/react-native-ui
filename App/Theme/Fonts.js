const size = {
  h1: 21,
  h2: 19,
  h3: 17,
  h4: 16,
  input: 15,
  regular: 14,
  medium: 14,
  small: 12
};

const style = {
  h1: {
    fontSize: size.h1
  },
  h2: {
    fontSize: size.h2
  },
  h3: {
    fontSize: size.h3
  },
  h4: {
    fontSize: size.h4
  },
  normal: {
    fontSize: size.regular
  }
};

export default {
  ...size,
  ...style
};
