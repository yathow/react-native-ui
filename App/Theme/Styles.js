import Metrics from './Metrics';

/**
 * This file defines the common styles.
 *
 * Use it to define generic component styles (e.g. the default text styles, default button styles...).
 */

export default {
  container: {
    flex: 1,
    flexDirection: 'column'
  },
  content: {
    flex: 1,
    flexDirection: 'column',
    padding: Metrics.md
  },
  bottomBtnPanel: {
    padding: Metrics.md
  },
  headerLbl: {
    paddingTop: Metrics.md,
    paddingBottom: Metrics.md
  }
};
