  
![Test](https://static.wixstatic.com/media/654e68_30c43b189a1f417eb2586908744da8aa~mv2.png/v1/fill/w_300,h_90,al_c,q_85,usm_0.66_1.00_0.01/logo_400x120.webp)

# React Native UI
------------------

Base React Native UI library for all react native to which contains:

* themes creator
* components

## Get started
------------------

### 1. Installation

Execute:  
>
    npm install @merchstack/react-native-ui


### 2. Usage

Import library at code and use component:  
>
    import { View Input } from '@merchstack/react-native-ui';
	...
    render() {
	    return (<View><Input placeholder={'Username'} value={this.state.username}/></View>);
    }
    ...
	
## Components supported
------------------	
* Avatar
* Badge
* Button
* ContentView
* Divider
* FlatList
* Header
* Icon
* Image
* Input
* Link
* ListItem
* LoadingIndicator
* NumberPicker
* Overlay
* Picker
* SafeAreaView
* SearchBar
* SearchBarHeader
* Snackbar
* Text
* View
	

## Customization
------------------

### 1. Theme

Create a theme object:
>
    import { createTheme } from '@merchstack/react-native-ui';
    ...
    const myTheme = createTheme({
	    colors: {
            primary: Colors.blue1,
            secondary: Colors.orange1,
            inverse: Colors.white,
            inputBorder: Colors.gray3,
            info: Colors.blue1,
            success: Colors.green1,
            error: Colors.red1,
            warning: Colors.orange2
        },
        backgroundColors: {
            main: Colors.white,
            primary: Colors.blue1,
            secondary: Colors.orange1
        },
        fonts: Fonts,
        metrics: Metrics
        }),
        dark: createTheme({
        fonts: Fonts,
        metrics: Metrics,
		...
		// Other customization
		...
    })	

Pass the theme to ThemeProvider:
>
    ...
    <ThemeProvider theme={myTheme}>
	...
